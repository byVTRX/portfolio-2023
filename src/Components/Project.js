// Components/Project.js
import React, { useState } from 'react';
import {
  Box,
  Image,
  Text,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  AspectRatio,
  Flex,
  useMediaQuery,
  useColorMode,
} from '@chakra-ui/react';

const Project = ({ title, subtitle, imageSrc, description }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isMobile] = useMediaQuery('(max-width: 768px)');
  const { colorMode, toggleColorMode } = useColorMode();
  const isDark = colorMode === "dark";

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  return (
    <Box
      position="relative"
      _hover={{ cursor: 'pointer', opacity: 0.8 }}
      onClick={openModal}
      width={isMobile ? "90%" : "30%" }
      m={2} 
    >
      <Image src={imageSrc} alt={title} borderRadius={20} style={{filter:"brightness(50%) blur(4px)"}}/>
      <Box
        position="absolute"
        top="50%"
        left="50%"
        transform="translate(-50%, -50%)"
        textAlign="center"
      >
        <Text fontSize="xl" fontWeight="bold" color={"white"}>
          {title}
        </Text>
        <Text fontSize="lg">{subtitle}</Text>
      </Box>

      <Modal isOpen={isOpen} onClose={closeModal}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color={isDark ? "white" : "black"}>{title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex>
              <AspectRatio ratio={4 / 3}>
                <Image src={imageSrc} alt={title} />
              </AspectRatio>
              <Box ml={4}>
                <Text fontSize="lg" fontWeight="bold" mb={2}>
                  {title}
                </Text>
                <Text>{description}</Text>
              </Box>
            </Flex>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export default Project;
