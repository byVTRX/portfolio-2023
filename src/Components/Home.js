// Components/Home.js
import React from 'react';
import { Box, Heading, Text, HStack, IconButton } from '@chakra-ui/react';
import { FaLinkedin, FaGithub } from 'react-icons/fa';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const Home = () => {
  return (
    <Box position="relative" height="100%" minHeight={"300px"}>
      <Carousel
        showArrows={false}
        showStatus={false}
        showIndicators={false}
        showThumbs={false}
        infiniteLoop={true}
        autoPlay={true}
        interval={5000}
        swipeable={false}
        dynamicHeight={true}
        
      >
        <Box height={"100%"}>
          <img src="./assets/contyfy.png" alt="Image 1" style={{filter:"blur(8px)",filter:"brightness(50%) blur(8px)"}}/>
        </Box>
        <Box height={"100%"}>
          <img src="./assets/p4.png" alt="Image 1" style={{filter:"blur(8px)",filter:"brightness(50%) blur(8px)"}}/>
        </Box>
        <Box height={"100%"}>
          <img src="./assets/portfolio.png" alt="Image 1" style={{filter:"blur(8px)",filter:"brightness(50%) blur(8px)"}}/>
        </Box>
      </Carousel>
      <Box
        position="absolute"
        top="50%"
        left="50%"
        transform="translate(-50%, -50%)"
        textAlign="center"
      >
        <Heading as="h2" size="xl">
          MARVIN BUTH
        </Heading>
        <Text fontSize="xl" mt={4}>
          Software Engineer
        </Text>
        <HStack mt={4} spacing={4} alignItems="center" justifyContent={"center"}>
          <IconButton
            as="a"
            href="https://www.linkedin.com/in/marvin-buth/"
            target="_blank"
            rel="noopener noreferrer"
            icon={<FaLinkedin />}
            aria-label="LinkedIn"
          />
          <IconButton
            as="a"
            href="https://github.com/MarvinButh"
            target="_blank"
            rel="noopener noreferrer"
            icon={<FaGithub />}
            aria-label="GitHub"
          />
        </HStack>
      </Box>
    </Box>
  );
};

export default Home;
