// Technologies.js
import React from 'react';
import {
  Box,
  Flex,
  Text,
  SimpleGrid,
  useColorModeValue,
} from '@chakra-ui/react';

import CIcon  from '@coreui/icons-react';
import { cibFirebase } from '@coreui/icons';

import { FaJava, FaHtml5, FaCss3, FaReact, FaNodeJs, FaDatabase, FaGitlab } from "react-icons/fa";
import { SiFirebase, SiPostgresql, SiOpenai, SiElectron, SiAwsamplify } from "react-icons/si";


const TechnologyBox = ({ icon, title }) => {
    const boxBackground = useColorModeValue("white", "black")
    return (
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        height="120px"
        bg={boxBackground}
        borderRadius="10px"
        shadow="lg"
        p="2"
    
      >
        {icon}
        <Text mt="2" fontSize="lg" fontWeight="semibold" textAlign="center">
          {title}
        </Text>
      </Box>
    );
  };

const Technologies = () => {
  return (
    <Box
      justifyContent="center"
      wrap="wrap"
      spacing={{ base: 4, md: 8 }}
      mt={{ base: 8, md: 12 }}
      mb={{ base: 8, md: 12 }}
    >
      <Text fontSize={{ base: "xl", md: "3xl" }} fontWeight="semibold" mb={{ base: "4", md: "8" }}>
        Technologies I've Worked With
      </Text>
      <br/>
      <SimpleGrid columns={{ base: 2, md: 4 }} spacing={{ base: "4", md: "8" }}>
        <TechnologyBox icon={<FaJava size="3em" />} title="Java" />
        <TechnologyBox icon={<FaHtml5 size="3em" />} title="HTML" />
        <TechnologyBox icon={<FaCss3 size="3em" />} title="CSS" />
        <TechnologyBox icon={<FaReact size="3em" />} title="React" />
        <TechnologyBox icon={<FaNodeJs size="3em" />} title="Node.js" />
        <TechnologyBox icon={<FaDatabase size="3em" />} title="MongoDB" />
        <TechnologyBox icon={<FaGitlab size="3em" />} title="GitLab" />
        <TechnologyBox icon={<SiFirebase size="3em" />} title="Firebase" />
        <TechnologyBox icon={<SiPostgresql size="3em" />} title="PostgresSQL" />
        <TechnologyBox icon={<SiOpenai size="3em" />} title="OpenAi GPT" />
        <TechnologyBox icon={<SiElectron size="3em" />} title="Electron" />
        <TechnologyBox icon={<SiAwsamplify size="3em" />} title="AWS Amplify" />
      </SimpleGrid>
      
    </Box>
  );
};

export default Technologies;
