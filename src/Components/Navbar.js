// Navbar.js
import React from 'react';
import {
  Box,
  Flex,
  Spacer,
  Text,
  useColorModeValue,
  useMediaQuery,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  HStack,
} from '@chakra-ui/react';
import { HamburgerIcon } from '@chakra-ui/icons';
import ColorModeSwitcher from './ColorModeSwitcher';

const scrollTo = (id) => {
  const element = document.getElementById(id);
  element.scrollIntoView({ behavior: 'smooth' });
};

const Navbar = () => {
  const bgColor = useColorModeValue('gray.200', 'gray.700');
  const textColor = useColorModeValue('gray.800', 'gray.100');
  const [isMobile] = useMediaQuery('(max-width: 768px)');

  const NavItems = () => (
    <Flex direction="row" spacing={4}>
      <Text margin={"0px 10px"} color={textColor} cursor="pointer" onClick={() => scrollTo('home')}>
        Home
      </Text>
      <Text margin={"0px 10px"} color={textColor} cursor="pointer" onClick={() => scrollTo('about')}>
        About
      </Text>
      <Text margin={"0px 10px"} color={textColor} cursor="pointer" onClick={() => scrollTo('contact')}>
        Contact
      </Text>
      
    </Flex>
  );

  return (
    <Box
      as="nav"
      position="fixed"
      top={0}
      left={0}
      right={0}
      bg={bgColor}
      p={4}
      zIndex={1}
      alignItems={"center"}
      verticalAlign={"center"}
    >
      <Flex>
        <Text mb={"auto"} mt={"auto"} color={textColor} fontWeight="bold">
          MARVIN BUTH
        </Text>
        <Spacer />
        {isMobile ? (
          <Menu>
             <ColorModeSwitcher/>
            <MenuButton as={HamburgerIcon} color={textColor} boxSize={6} />
            <MenuList>
              <MenuItem onClick={() => scrollTo('home')}>Home</MenuItem>
              <MenuItem onClick={() => scrollTo('about')}>About</MenuItem>
              <MenuItem onClick={() => scrollTo('contact')}>Contact</MenuItem>
             
            </MenuList>
            
          </Menu>
        ) : (
            <Box>
                <HStack>
                <ColorModeSwitcher/>
            <NavItems />
                </HStack>
            
            </Box>
       
        )}
      </Flex>
    </Box>
  );
};

export default Navbar;
