// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

import { Box, Heading, Text, Link, Image } from "@chakra-ui/react";

const ProjectSlide = ({ project }) => {
  return (
    <Box textAlign="center">
      <Image src={project.image} alt={project.title} />
      <Box p="4">
        <Heading as="h2" size="md">
          {project.title}
        </Heading>
        <Text>{project.description}</Text>
        <Link href={project.link}>View Project</Link>
      </Box>
    </Box>
  );
};

const projects = [
    {
      title: "Project 1",
      description: "This is the first project",
      image: "https://picsum.photos/1920/1080",
      link: "#",
    },
    {
      title: "Project 2",
      description: "This is the second project",
      image: "https://picsum.photos/1920/1080",
      link: "#",
    },
    {
      title: "Project 3",
      description: "This is the third project",
      image: "https://picsum.photos/1920/1080",
      link: "#",
    },
  ];

  const sliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  const ProjectCarousel = ({ images }) => {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
    };
  
    return (
      <Box>
        <Slider {...settings}>
          {projects.map((project, index) => (
            <Box key={index}>
              <ProjectSlide project={project}/>
            </Box>
          ))}
        </Slider>
      </Box>
    );
  };
  
  export default ProjectCarousel;