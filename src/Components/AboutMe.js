// AboutMe.js
import React from 'react';
import { Box, Flex, Text } from '@chakra-ui/react';

const AboutMe = () => {
  return (
    <Box alignItems={"center"} maxWidth="80%">
      <Text fontSize={{ base: 'sm', md: 'md', lg: 'lg' }}  textAlign="center" lineHeight="1.6">
        I am a software developer with 8 years of experience. My journey started
        with learning Java to create extensions for the popular sandbox game
        Minecraft. In 2017, I founded my own development agency at just 14 years
        old, which was successful until 2019 when I had to close it down to
        focus on school. During my internship at the European Central Bank, I
        learned about payment infrastructure and blockchain technology. Since
        2019, I have been working as a Full-Stack contract developer, focusing
        on the MEAN stack. I have also been one of the <b>first</b> people in the 
        world to gain access to OpenAI's GPT beta back in 2021, which means I know
        the model very well and can use it flawlessly.
      </Text>
    </Box>
  );
};

export default AboutMe;
