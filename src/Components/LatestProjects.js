// Components/LatestProjects.js
import { Box, Button, Heading, HStack, Text, useMediaQuery, VStack } from '@chakra-ui/react';
import React from 'react';
import Project from './Project';

const LatestProjects = ({ bgColor, textColor }) => {

    const [isMobile] = useMediaQuery('(max-width: 768px)');
    return (

        <Box className="section" mb={isMobile ? "10" : "10"} mt={isMobile ? "10" : "0"} bg={bgColor} color={textColor} id="projects">
            <Heading as="h1" mt="0">Latest Projects</Heading>
            <Box
                d="flex"
                justifyContent="center"
                flexWrap="wrap"
                mt={0}
                mx={-2}
            >
                {!isMobile ?
                    (<HStack
                        justifyContent="center"
                    >
                        <Project
                            title={<Text>Pengueen v4</Text>}
                            subtitle={<Text color={"gray.100"}>Make common workflows digital with Pengueen</Text>}
                            imageSrc="./assets/p4.png"
                            description={
                                <Box mb={10} mt={0} justifyContent={"center"} textAlign={"center"}>
                                    <Text>Pengueen is a company focused on providing an ethical digitalization platform, making digitalization accessible, enjoyable, and participatory for everyone. Their approach emphasizes digital tools as instruments for professionals, designed, created, and maintained by the experts themselves. This is made possible through their innovative software construction kit.</Text>
                                    <br />
                                    <Text>A key aspect of Pengueen's mission is to ensure a positive digitalization balance, meaning that the benefits of using their tools should outweigh the efforts required for their maintenance and creation. This principle is at the heart of their work and sets them apart in the digitalization landscape.</Text>
                                    <br />
                                    <Text>During my time at Pengueen, I mainly worked on the backend of the project, utilizing the popular NodeJS framework NestJS, along with PostgreSQL for data storage and TypeScript for programming. I also contributed to the frontend, leveraging ReactJS and cutting-edge UI frameworks to deliver an exceptional user experience.</Text>
                                    <br />
                                    <Button onClick={(e) => window.open('https://p4.pengueen.de/', '_blank').focus()}>Have a look at Pengueen v4 here!</Button>
                                </Box>} />
                        <Project
                            title={<Text >CONTYFY</Text>}
                            subtitle={<Text color={"gray.100"}>Online platform for digital marketing and content creation</Text>}
                            imageSrc="./assets/contyfy.png"
                            description={<Box mb={10} mt={0} justifyContent={"center"} textAlign={"center"}>
                                <Text>One of my biggest projects to this day is the content sharing network Contyfy. Even though becuase of intern problems with the management the platform never went live, it has been an awesome project to work on.</Text>
                                <br />
                                <Text>Contyfy's mission was making it easier for content creators who want to make money with their content. Thorugh paid subscription models or the implementation of a tip system on posts, as well as even paid live stream content this platform had everything a creator could wish for.</Text>
                                <br />
                                <Text>To achieve maximum stability even under heavy load, I used microservices for this project. For horizontal scalability, I used Googles FIREBASE. A service that scale the architecture infinitely depending on demand. For payment issues I used the stripe API for this projcet, which allowed me to accept many diffrent forms of payment including crypto, credit card, apple/google pay and more...
                                    For the backend environment I used Google CloudFunctions through firebase with NodeJS and express. this allowed for a modular setup and an easy implementation of the Stripe API as well as everything else necessary. The front end was made with vanilla ReactJS and the Material UI design library, that provided a clean look and a native mobile support.
                                    <br />
                                    As this is a solo project where I handled all of the steps on my own, I consider this to be my biggest project and I am very proud of it!
                                </Text>
                                <br />
                                <Button onClick={(e) => window.open('https://contyfy.com/', '_blank').focus()}>Have a look at Contyfy here!</Button>
                            </Box>}
                        />
                        <Project
                            title={<Text >This porfolio</Text>}
                            subtitle={<Text color={"gray.100"}>Clean design using the React Framework and ChakraUI</Text>}
                            imageSrc="./assets/portfolio.png"
                            description={<Box mb={10} mt={0} justifyContent={"center"} textAlign={"center"}>
                                <Text>This portfolio is my most recent project yet. I am aiming to show of my work as well as my design skills with this beautiful <b>ReactJS</b> website.</Text>
                                <br />
                                <Text>The core of this portfolio page was built in just a few hours with much love for detail and even a dark/light mode!</Text>
                                <br />
                                <Text>My mission for this page was to create a good looking UI that is functional both on mobile and desktop. I used as many of ReactJS's tricks to make the page as intuitive and user friendly as possible.
                                    The use of state variables is how I made the switch from dark to light mode as easy as possible with just one click.
                                    <br />
                                    The design library <b>ChakraUI</b> helped me achieve this gorgous UI that looks so good even on mobile devices!
                                    <br />Using ReactJS's <b>stores</b> and Google's <b>Firebase</b> to store any data passed to the contact form I have easy access to all of the incoming contact requests


                                    <br />
                                    The code of this portfolio page is live on GitLab!</Text>
                                <br />
                                <Button onClick={(e) => window.open('https://gitlab.com/byVTRX/portfolio-2023', '_blank').focus()}>You can have a look at the code here</Button>
                            </Box>} />
                    </HStack>) :
                    (<VStack
                        justifyContent="center"
                    >
                        <Project
                            title={<Text>Pengueen v4</Text>}
                            subtitle={<Text color={"gray.100"}>Make common workflows digital with Pengueen</Text>}
                            imageSrc="./assets/p4.png"
                            description={
                                <Box mb={10} mt={0} justifyContent={"center"} textAlign={"center"}>
                                    <Text>Pengueen is a company focused on providing an ethical digitalization platform, making digitalization accessible, enjoyable, and participatory for everyone. Their approach emphasizes digital tools as instruments for professionals, designed, created, and maintained by the experts themselves. This is made possible through their innovative software construction kit.</Text>
                                    <br />
                                    <Text>A key aspect of Pengueen's mission is to ensure a positive digitalization balance, meaning that the benefits of using their tools should outweigh the efforts required for their maintenance and creation. This principle is at the heart of their work and sets them apart in the digitalization landscape.</Text>
                                    <br />
                                    <Text>During my time at Pengueen, I mainly worked on the backend of the project, utilizing the popular NodeJS framework NestJS, along with PostgreSQL for data storage and TypeScript for programming. I also contributed to the frontend, leveraging ReactJS and cutting-edge UI frameworks to deliver an exceptional user experience.</Text>
                                    <br />
                                    <Button onClick={(e) => window.open('https://p4.pengueen.de/', '_blank').focus()}>Have a look at Pengueen v4 here!</Button>
                                </Box>} />
                        <Project
                            title={<Text >CONTYFY</Text>}
                            subtitle={<Text color={"gray.100"}>Online platform for digital marketing and content creation</Text>}
                            imageSrc="./assets/contyfy.png"
                            description={<Box mb={10} mt={0} justifyContent={"center"} textAlign={"center"}>
                                <Text>One of my biggest projects to this day is the content sharing network Contyfy. Even though becuase of intern problems with the management the platform never went live, it has been an awesome project to work on.</Text>
                                <br />
                                <Text>Contyfy's mission was making it easier for content creators who want to make money with their content. Thorugh paid subscription models or the implementation of a tip system on posts, as well as even paid live stream content this platform had everything a creator could wish for.</Text>
                                <br />
                                <Text>To achieve maximum stability even under heavy load, I used microservices for this project. For horizontal scalability, I used Googles FIREBASE. A service that scale the architecture infinitely depending on demand. For payment issues I used the stripe API for this projcet, which allowed me to accept many diffrent forms of payment including crypto, credit card, apple/google pay and more...
                                    For the backend environment I used Google CloudFunctions through firebase with NodeJS and express. this allowed for a modular setup and an easy implementation of the Stripe API as well as everything else necessary. The front end was made with vanilla ReactJS and the Material UI design library, that provided a clean look and a native mobile support.
                                    <br />
                                    As this is a solo project where I handled all of the steps on my own, I consider this to be my biggest project and I am very proud of it!
                                </Text>
                                <br />
                                <Button onClick={(e) => window.open('https://contyfy.com/', '_blank').focus()}>Have a look at Contyfy here!</Button>
                            </Box>}
                        />
                        <Project
                            title={<Text >This porfolio</Text>}
                            subtitle={<Text color={"gray.100"}>Clean design using the React Framework and ChakraUI</Text>}
                            imageSrc="./assets/portfolio.png"
                            description={<Box mb={10} mt={0} justifyContent={"center"} textAlign={"center"}>
                                <Text>This portfolio is my most recent project yet. I am aiming to show of my work as well as my design skills with this beautiful <b>ReactJS</b> website.</Text>
                                <br />
                                <Text>The core of this portfolio page was built in just a few hours with much love for detail and even a dark/light mode!</Text>
                                <br />
                                <Text>My mission for this page was to create a good looking UI that is functional both on mobile and desktop. I used as many of ReactJS's tricks to make the page as intuitive and user friendly as possible.
                                    The use of state variables is how I made the switch from dark to light mode as easy as possible with just one click.
                                    <br />
                                    The design library <b>ChakraUI</b> helped me achieve this gorgous UI that looks so good even on mobile devices!
                                    <br />Using ReactJS's <b>stores</b> and Google's <b>Firebase</b> to store any data passed to the contact form I have easy access to all of the incoming contact requests


                                    <br />
                                    The code of this portfolio page is live on GitLab!</Text>
                                <br />
                                <Button onClick={(e) => window.open('https://gitlab.com/byVTRX/portfolio-2023', '_blank').focus()}>You can have a look at the code here</Button>
                            </Box>} />

                    </VStack>)
                }


            </Box>
        </Box >
    );
};

export default LatestProjects;
