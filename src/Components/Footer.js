// Footer.js
import React from 'react';
import { Box, useColorModeValue } from '@chakra-ui/react';
import ImprintModal from './ImprintModal';

const Footer = () => {
  const bgColor = useColorModeValue('gray.200', 'gray.700');
  const textColor = useColorModeValue('gray.800', 'gray.100');

  return (
    <Box
      as="footer"
      position="absolute"
      bottom={0}
      left={0}
      right={0}
      bg={bgColor}
      color={textColor}
      p={4}
      textAlign="center"
      
    >
      &copy; {new Date().getFullYear()} MARVIN BUTH. All rights reserved.
      <br/>
      <ImprintModal/>
    </Box>
  );
};

export default Footer;