// ColorModeSwitcher.js
import * as React from "react";
import { IconButton, useColorMode, useMediaQuery } from "@chakra-ui/react";
import { SunIcon, MoonIcon } from "@chakra-ui/icons"

const ColorModeSwitcher = (props) => {
  const { colorMode, toggleColorMode } = useColorMode();
  const isDark = colorMode === "dark";
  const icon = isDark ? <SunIcon/> : <MoonIcon/>;
  const label = isDark ? "Switch to light mode" : "Switch to dark mode";
  const [isMobile] = useMediaQuery('(max-width: 768px)');

  return (
    <IconButton
      size={isMobile? "sm" : "md"}
      fontSize="lg"
      aria-label={label}
      variant="ghost"
      color="current"
      padding={0}
      
      onClick={toggleColorMode}
      icon={icon}
      {...props}
    />
  );
};

export default ColorModeSwitcher;
