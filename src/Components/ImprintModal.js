import React, { useState } from "react";
import {
    Box,
    Link,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Heading,
    Text,
} from "@chakra-ui/react";

const ImprintModal = () => {
    const [isOpen, setIsOpen] = useState(false);

    const handleOpenModal = () => {
        setIsOpen(true);
    };

    const handleCloseModal = () => {
        setIsOpen(false);
    };

    return (
        <Box>
            <Link onClick={handleOpenModal}>Imprint</Link>

            <Modal isOpen={isOpen} onClose={handleCloseModal} size="xl">
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Imprint</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Heading as="h2" size="md" mb={4}>
                            Legal Information
                        </Heading>
                        <Text>
                            Marvin Buth <br />
                            Neue Str. 32b <br />
                            61250 Usingen <br />
                            Germany <br />
                            <br />
                            Phone: <br/> upon request <br />
                            E-Mail: <br/>
                            <Link href="mailto:mail@marvinbuth.de/>" isExternal>
                                mail@marvinbuth.de
                            </Link>
                            <br />

                            <br />
                            VAT identification number according to § 27a of Value Added Tax
                            Act: <br/>DE 355 473 014 <br />
                            <br />
                            Responsible for content according to § 55 paragraph 2 RStV: <br/>
                            Marvin Buth <br />
                            <br />
                            <br />
                            <br />
                           

                            <Heading as="h2" size="md" mb={4}>
                                Disclaimer
                            </Heading>
                            <Text>
                                <strong>Accountability for content</strong>
                                <br />
                                The contents of our pages have been created with the utmost care.
                                However, we cannot guarantee the contents' accuracy, completeness
                                or topicality. According to statutory provisions, we are
                                furthermore responsible for our own content on these web pages.
                                In this matter, please note that we are not obliged to monitor
                                the transmitted or saved information of third parties, or
                                investigate circumstances pointing to illegal activity. Our
                                obligations to remove or block the use of information under
                                generally applicable laws remain unaffected by this as per
                                §§ 8 to 10 of the Telemedia Act (TMG).
                                <br />
                                <br />
                                <strong>Accountability for links</strong>
                                <br />
                                Responsibility for the content of external links (to web pages of
                                third parties) lies solely with the operators of the linked
                                pages. No violations were evident to us at the time of linking.
                                Should any legal infringement become known to us, we will remove
                                the respective link immediately.
                                <br />
                                <br />
                                <strong>Copyright</strong>
                                <br />
                                Our web pages and their contents are subject to German copyright
                                law. Unless expressly permitted by law, every form of utilizing,
                                reproducing or processing works subject to copyright
                                protection on our web pages requires the prior consent of the
                                respective owner of the rights. Individual reproductions of a
                                work are only allowed for private use. The materials from these
                                pages are copyrighted.
                                <br />
                                <br />
                                <strong>Data Protection</strong>
                                <br />
                                Please be aware that there are inherent security risks in
                                transmitting data, such as e-mails, via the Internet, because it
                                is impossible to safeguard completely against unauthorized
                                access by third parties. Nevertheless, we shall safeguard your
                                data, subject to this limitation. In particular, personal
                                information will be transmitted via the Internet only if it does
                                not infringe upon third-party rights, unless the respective
                                party has given its prior consent in view of such security
                                risks. Accordingly, as the Web site provider, we shall not be
                                held liable for any damages incurred as a consequence of such
                                security risks or for any related acts of omission on our part.
                                We oppose the use of any available contact information by a
                                third party for sending unsolicited advertisements. As the Web
                                site provider, we reserve the express right to take legal action
                                against unsolicited mailing or e-mailing of spam and other
                                similar advertising materials.
                            </Text>
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                        </Text>
                    </ModalBody>
                </ModalContent>
            </Modal>
        </Box>
    );
};

export default ImprintModal;
