import React, { useState, useContext } from "react";
import { Box, Input, Button, Textarea } from "@chakra-ui/react";
import { ContactFormContext } from "../Store/ContactFormContext";

const ContactForm = () => {
  const { sendContactForm } = useContext(ContactFormContext);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = { name, email, message };
    sendContactForm(formData);
    // Reset form fields
    setName("");
    setEmail("");
    setMessage("");
  };

  return (
    <Box>
      <form onSubmit={handleSubmit}>
        <Input
          type="text"
          placeholder="Your Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          mb={4}
          required
        />
        <Input
          type="email"
          placeholder="Your Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          mb={4}
          required
        />
        <Textarea
          placeholder="Your Message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          mb={4}
          required
        />
        <Button type="submit" colorScheme="blue">
          Submit
        </Button>
      </form>
    </Box>
  );
};

export default ContactForm;
