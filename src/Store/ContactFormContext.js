import React, { createContext, useState } from "react";

// Import the handler function from the functions folder
// Create the ContactForm context
export const ContactFormContext = createContext();

// Create a component to provide the context
const ContactFormProvider = ({ children }) => {







  // Create a function to send the form data to the handler function using fetch
  const sendContactForm = async (formData) => {
    try {
      const response = await fetch(".netlify/functions/createContactRequest", {
        method: "POST",
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ContactFormContext.Provider value={{ sendContactForm }}>
      {children}
    </ContactFormContext.Provider>
  );
};

export default ContactFormProvider;
