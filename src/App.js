// App.js
import React from "react";
import {
  Box,
  HStack,
  Heading,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import ColorModeSwitcher from "./Components/ColorModeSwitcher";
import "./App.css";
import Footer from "./Components/Footer";
import Navbar from "./Components/Navbar";
import AboutMe from "./Components/AboutMe";
import Technologies from "./Components/Technologies";
import Home from "./Components/Home";
import ProjectCarousel from "./Components/ProjectCarousel";
import LatestProjects from "./Components/LatestProjects";
import ContactFormProvider from "./Store/ContactFormContext";
import ContactForm from "./Components/ContactForm";

const App = () => {
  const { colorMode } = useColorMode();
  const textColor = useColorModeValue("gray.800", "gray.100");
  
  const section2BgColor = useColorModeValue("#ced4da", "#181828");
  const section3BgColor = useColorModeValue("#adb5bd", "#0D0C18");
  const navColor = useColorModeValue("gray.200", "gray.800");

  return (
    <>
    <Box textAlign="center" fontSize="xl">
     <Navbar/>
      
        <Home/>
       
      <Box className="section" bg={section2BgColor} color={textColor} id="about">
        <Heading as="h1">About Me</Heading>
        <AboutMe/>
        <Technologies/>
      </Box>
      <LatestProjects/>
      <Box className="section" bg={section3BgColor} color={textColor} id="contact">
        <Heading as="h1" mb={10}>Contact me</Heading>
        <ContactFormProvider>
      <ContactForm />
    </ContactFormProvider>
      </Box>
      
    </Box>
    <Footer className="section"/>
    </>
  );
};

export default App;
