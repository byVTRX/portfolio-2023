// Docs on event and context https://docs.netlify.com/functions/build/#code-your-function-2
//Firebase-Firestore import
const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);


initializeApp({
  credential: cert(JSON.parse(process.env.SERVICE_ACCOUNT))
});

const db = getFirestore();

const handler = async (event) => {

  const params = JSON.parse(event.body);
  console.log(params);

  
 
  if(params.name && params.email && params.message){




    // Add a new document with a generated id.
    const ref = await db.collection('contactRequests').add({
      name: params.name,
      email: params.email,
      message: params.message,
      createdAt: FieldValue.serverTimestamp()
      });


    

    if(ref.id){
      console.log("Document written with ID: ", ref.id);

      //SEND EMAIL TO ME
      
      const msg = {
        to: 'marvinbuth3@gmail.com',
        from: 'mail@marvinbuth.de',
        subject: 'New Contact Request',
        text: `Name: ${params.name} \n Email: ${params.email} \n Message: ${params.message}`,
        html: `<strong>Name: ${params.name} <br> Email: ${params.email} <br> Message: ${params.message}</strong>`,
      };

      //SEND EMAIL TO USER
      const msg2 = {
        to: params.email,
        from: 'mail@marvinbuth.de',
        subject: 'Thank you for your message',
        text: `Hello ${params.name}, \n I received your message. Thank you! \n \n Best regards, \n Marvin Buth`,
        html: `<strong>Hello ${params.name}, <br> I received your message. Thank you! <br> <br> Best regards, <br> Marvin Buth</strong>`,
      };

      try {
        await sgMail.send(msg);
        await sgMail.send(msg2);
      }
      catch (error) {
        console.error(error);
        if (error.response) {
          console.error(error.response.body)
        }
      }

      



      try {
        return {
          statusCode: 200,
          body: JSON.stringify({ message: `I received your message. Thank you ${params.name}!` }),
          // // more keys you can return:
          // headers: { "headerName": "headerValue", ... },
          // isBase64Encoded: true,
        }
      } catch (error) {
        console.log(error);
        return {
          statusCode: 500,
          body: JSON.stringify({ message: `Error: ${error}` }),
        }
      }
      

      
    } else {
      console.log("Error: Could not create document");
    }
  } else {
    console.log("Error: Missing parameters");
  }



}

module.exports = { handler }
